export function jsonify(form)
{
    var field, l, s = [];

    if (typeof form == 'object' && form.nodeName === "FORM") {
        var len = form.elements.length;

        for (var i = 0; i < len; i++) {
            field = form.elements[i];
            if (field.name && !field.disabled && field.type !== 'button' && field.type !== 'file' &&
                field.type !== 'hidden' && field.type !== 'reset' && field.type !== 'submit') {
                if (field.type === 'select-multiple') {
                    l = form.elements[i].options.length;

                    for (var j = 0; j < l; j++) {
                        if (field.options[j].selected) {
                            s[s.length] = field.name + "=" + field.options[j].value;
                        }
                    }
                }
                else if ((field.type !== 'checkbox' && field.type !== 'radio') || field.checked) {
                    s[s.length] = field.name + "=" + field.value;
                }
            }
        }
    }
    s.join('&').replace(/%20/g, '+');
    let obj = {};
    for(let i = 0; i < s.length; ++i)
    {
        obj[s[i].split('=')[0]] = s[i].split('=')[1];
    }

    return JSON.stringify(obj);
}
export function getJSessionId(){
    var jsId = document.cookie.match(/JSESSIONID=[^;]+/);
    if(jsId != null) {
        if (jsId instanceof Array)
            jsId = jsId[0].substring(11);
        else
            jsId = jsId.substring(11);
    }
    return jsId;
}

export function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

export function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

export function checkCookie() {
    var user = getCookie("username");
    if (user !== "") {
        alert("Welcome again " + user);
    } else {
        user = prompt("Please enter your name:", "");
        if (user !== "" && user != null) {
            setCookie("username", user, 365);
        }
    }
}
