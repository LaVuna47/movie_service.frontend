/*
 Classes that mirrors DB representation of tables
* */
export class Movie {
    constructor() {
        this.id = 0;
        this.name = "no name";
        this.picture = "https://asianwiki.com/images/8/88/Default-KM-engsubtrailer.jpg";
        this.info = "...";
        this.actors = "no actors";
        this.duration = "0:0:0";
    }
    setMovie(id, name, picture, info, actors, duration) {
        this.id = id;
        this.name = name;
        this.picture = picture;
        this.info = info;
        this.actors = actors;
        this.duration = duration;
    }
    serialise() { // returns json from this object
        return JSON.stringify(this);
    }
}
export class User{
    constructor() {
        this.id = 0;
        this.username = "anonim";
        this.firstname = "anonim";
        this.lastname = "anonim";
        this.email = "anonim@gmail.com";
        this.password_hash = "password";
        this.phone_numer = "none";
        this.photo = "no_profile.jpeg";
        this.role = "user";
    }
    setUser(id, username, firstname, lastname, email, password_hash, phone_number, photo, role){
        this.id = id;
        this.username = username;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.password_hash = password_hash;
        this.phone_numer = phone_number;
        this.photo = photo;
        this.role = role;
    }
    serialise() {
        return JSON.stringify(this);
    }
}

export class Reservation{
    constructor() {
        this.id = 0;
        this.movie_schedule_id = 0;
        this.user_id = 0;
    }
    setReservation(id, movie_schedule_id, user_id){
        this.id = id;
        this.movie_schedule_id = movie_schedule_id;
        this.user_id = user_id;
    }
    serialise() {
        return JSON.stringify(this);
    }
}

export class MovieSchedule{
    constructor() {
        let today = new Date();

        this.id = 0;
        this.date = today.toJSON().slice(0,10).replace(/-/g,'/');
        this.time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        this.movie_id = 0;
    }
    setMovieSchedule(id, date, time, movie_id){
        this.id = id;
        this.date = date;
        this.time = time;
        this.movie_id = movie_id;
    }
    serialise() {
        return JSON.stringify(this);
    }
}
