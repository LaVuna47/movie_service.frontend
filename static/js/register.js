import {jsonify} from "./general/Base.js";

function registerUser(e, form){
    e.preventDefault();
    let http = new XMLHttpRequest();
    http.open('POST',"https://127.0.0.1:5000/user/register",true);
    http.setRequestHeader("Content-Type", "application/json");
    http.onload = function(){
        alert(http.responseText);
    }

    let data = jsonify(form);
    http.send(data);
    return false;
}

function main() {
    let form = document.getElementById("register-form");
    form.onsubmit = function (e){
        registerUser(e,form);
    };
}
main();

