import { jsonify } from "./general/Base.js";
import {getJSessionId} from "./general/Base.js";
import {setCookie} from "./general/Base.js"
import {getCookie} from "./general/Base.js"
import {checkCookie} from "./general/Base.js"

function loginUser(e, form){
    e.preventDefault();
    let http = new XMLHttpRequest();
    http.open('POST',"https://127.0.0.1:5000/user/login",true);
    http.setRequestHeader("Content-Type", "application/json");
    let data = jsonify(form);
    let password = JSON.parse(data)["password"];
    http.onload = function(){
        alert(http.responseText);

        let obj = JSON.parse(http.responseText)
        setCookie('email', obj["User"]["email"],1);
        setCookie('id', obj["User"]["id"],1);
        setCookie('password', password,1);
        setCookie("username",obj["User"]["username"]);
        // console.log("Email: ", getCookie("email"));
        // console.log("id: ", getCookie("id"));
        // console.log("Password: ", getCookie("password"));

    }

    http.send(data);
    return false;
}

function main() {
    let form = document.getElementById("login-form");
    form.onsubmit = function (e){
        loginUser(e,form);
    };
}
main();
