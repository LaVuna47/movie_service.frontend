import {MovieManager} from "./general/Movie.js";


function addMovies(){
    let el = document.getElementById("movie-container1");
    let movie_manager = new MovieManager();
    let movies = movie_manager.getAllMovies();

    for (let movie of movies) {
        let box = document.createElement("div");
        let img = document.createElement("img");
        let div1 = document.createElement("div");
        let div2 = document.createElement("div");
        let div3 = document.createElement("div");

        div1.innerText="Name: " + movie.name;
        div2.innerText="actors: " + movie.actors;
        div3.innerText="duration: " + movie.duration;

        img.src = movie.picture;
        img.alt = div1.innerText;

        box.classList.add("movie-box");
        box.appendChild(img);
        box.appendChild(div1);
        box.appendChild(div2);
        box.appendChild(div3);

        el.appendChild(box);
    }

}

function main()
{
    addMovies();
}



// calling main function
main();

