import {setCookie} from "./general/Base.js"
import {getCookie} from "./general/Base.js"
import {checkCookie} from "./general/Base.js"

/// TODO : 1) display use info on profile page
/// TODO : 2) add logout button into profile page
function getUser(){
    console.log("id: ", getCookie("id"));
    console.log("username: ",getCookie("username"));
    console.log("password: ",getCookie("password"));

    let http = new XMLHttpRequest();
    let url = "https://127.0.0.1:5000/user/" + getCookie("id").toString();
    http.open("GET",url,true,getCookie("username"), getCookie("password"));
    http.setRequestHeader("Content-Type", "application/json");
    http.setRequestHeader ("Authorization", "Basic " + btoa(getCookie("username")+ ":" +  getCookie("password")));

    http.onload = function () {
        // getting user
        let user = JSON.parse(http.responseText);
        // setting user profile
        let username = document.getElementById("u-name");
        let realname = document.getElementById("r-name");
        let secondname = document.getElementById("s-name");
        let email = document.getElementById("e-mail");
        let profileImg = document.getElementById("profile-img");
        username.innerText = user["User"]["username"];
        realname.innerText = user["User"]["firstname"];
        secondname.innerText = user["User"]["lastname"];
        email.innerText = user["User"]["email"];
        profileImg.src = user["User"]["photo"];
    }
    http.send();
}

document.getElementById("logout-btn").onclick = function (){
    document.cookie.split(";").forEach(function(c) { document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/"); });
}


getUser();

